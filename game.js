$(window).load(function () {
    var textArray = ["Your game sucks", "Your game blows", "Your game is a disaster", "Your game is unoriginal", "Your game is not special", "Your game is pretentious", "Your game is boring", "Your game is a ripoff", "Your game was already made", "Your game is evil", "Your game breaks heart", "Your game is kind", "Your game should have never been made", "Will you now register trademarks?"
],
        currentHash = window.location.hash.split('#')[1];
    currentText = currentHash !== undefined ? textArray[textArray.indexOf(currentHash.replace(/\-/g, ' '))] : undefined;
    $('#iFrm').randomText(textArray, 1000000, '', undefined, currentText);
});

$.fn.randomText = function (textArray, interval, randomEle, prevText, currentText) {
    var obj = $(this);
    if ($('#iContent').length == 0) {
        obj.append('<div id="iContent">');
    }
    var textCont = $('#iContent');
    if (typeof randomEle != 'undefined') {
        if ($('#refresh').length == 0) {
            obj.append('<a href="javascript:;" id="refresh"><em>' + randomEle);
        }
    }
    textCont.fadeOut(700, function () {
        var chosenText = (currentText !== undefined) ? currentText : random_array(textArray);
        while (chosenText == prevText) {
            chosenText = random_array(textArray);
        }
		
		/* tweet button plug */
		// remove tweet button iframe
		$('#tweetButton iframe').remove();
		// Generate new markup
		var tb = $('<a></a>')
			.addClass('twitter-share-button')
			.attr('href', 'http://twitter.com/share')
			.attr('data-url', 'http://www.howgoodismygame.com')
			.attr('data-via', 'teamhash')
			.attr('data-text', "How good is my game? - " + chosenText + " #howgoodismygame");
		$('#tweetButton').append(tb);
		twttr.widgets.load();

		
        textCont.empty().html(chosenText);
        textCont.fadeIn(500);
        sendText = chosenText;
        window.location.hash = sendText.replace(/\s/g, '-');
    });
    timeOut = setTimeout(function () {
        obj.randomText(textArray, interval, randomEle, sendText);
    }, interval);
    $("#refresh").click(function () {
        if (!textCont.is(':animated')) {
            clearTimeout(timeOut);
            obj.randomText(textArray, interval, randomEle, sendText);
        }
    });
}

function random_array(aArray) {
    var rand = Math.floor(Math.random() * aArray.length + aArray.length);
    var randArray = aArray[rand - aArray.length];
    return randArray;
}